# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<HeaderNavigation>` | `<header-navigation>` (components/HeaderNavigation.vue)
- `<Logo>` | `<logo>` (components/Logo.vue)
- `<NuxtLogo>` | `<nuxt-logo>` (components/NuxtLogo.vue)
- `<BaseInput>` | `<base-input>` (components/base/BaseInput.vue)
- `<ListRegistries>` | `<list-registries>` (components/list/ListRegistries.vue)
- `<ListSpecificUser>` | `<list-specific-user>` (components/list/ListSpecificUser.vue)
- `<ListUsers>` | `<list-users>` (components/list/ListUsers.vue)
- `<MainModal>` | `<main-modal>` (components/main/MainModal.vue)
