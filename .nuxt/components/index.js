export { default as HeaderNavigation } from '../../client/components/HeaderNavigation.vue'
export { default as Logo } from '../../client/components/Logo.vue'
export { default as NuxtLogo } from '../../client/components/NuxtLogo.vue'
export { default as BaseInput } from '../../client/components/base/BaseInput.vue'
export { default as ListRegistries } from '../../client/components/list/ListRegistries.vue'
export { default as ListSpecificUser } from '../../client/components/list/ListSpecificUser.vue'
export { default as ListUsers } from '../../client/components/list/ListUsers.vue'
export { default as MainModal } from '../../client/components/main/MainModal.vue'

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
