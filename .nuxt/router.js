import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _7b80a368 = () => interopDefault(import('../client/pages/n_index.vue' /* webpackChunkName: "pages/n_index" */))
const _61dde59d = () => interopDefault(import('../client/pages/index.vue' /* webpackChunkName: "pages/index" */))
const _45e0ea08 = () => interopDefault(import('../client/pages/_id/index.vue' /* webpackChunkName: "pages/_id/index" */))
const _4eb28d85 = () => interopDefault(import('../client/pages/_id/user.vue' /* webpackChunkName: "pages/_id/user" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/n_index",
    component: _7b80a368,
    name: "n_index"
  }, {
    path: "/",
    component: _61dde59d,
    name: "index"
  }, {
    path: "/:id",
    component: _45e0ea08,
    name: "id"
  }, {
    path: "/:id/user",
    component: _4eb28d85,
    name: "id-user"
  }, {
    path: "/tabs/list-users",
    component: _61dde59d,
    props: {"tab":0},
    name: "ListUsers"
  }, {
    path: "/tabs/list-registries",
    component: _61dde59d,
    props: {"tab":1},
    name: "ListRegistries"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
