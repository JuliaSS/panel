import { NestFactory } from '@nestjs/core';
import { AppModule } from './src/app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.init();
  await app.enableCors()
  return app.getHttpAdapter().getInstance();
}

export default bootstrap;