export function extendRoutes(routes, resolve) {
    routes.push({
        name: 'ListUsers',
        path: '/tabs/list-users',
        component: resolve(__dirname, '../pages/index.vue'),
        props: {
            tab: 0
        }
    })
    routes.push({
        name: 'ListRegistries',
        path: '/tabs/list-registries',
        component: resolve(__dirname, '../pages/index.vue'),
        props: {
            tab: 1
        }
    })
}